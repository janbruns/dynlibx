{
  $XFree86: xc/lib/Xrandr/Xrandr.h,v 1.9 2002/09/29 23:39:44 keithp Exp $
 
  Copyright (C) 2000 Compaq Computer Corporation, Inc.
  Copyright (C) 2002 Hewlett-Packard Company, Inc.
 
  Permission to use, copy, modify, distribute, and sell this software and its
  documentation for any purpose is hereby granted without fee, provided that
  the above copyright notice appear in all copies and that both that
  copyright notice and this permission notice appear in supporting
  documentation, and that the name of Compaq not be used in advertising or
  publicity pertaining to distribution of the software without specific,
  written prior permission.  HP makes no representations about the
  suitability of this software for any purpose.  It is provided "as is"
  without express or implied warranty.
 
  HP DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL COMPAQ
  BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 
  Author:  Jim Gettys, HP Labs, HP.
}

unit xrandr;

interface

{$PACKRECORDS c}

uses
  ctypes, x, xlib;

const
  libXrandr = 'libXrandr.so';

{$I randr.inc}

type
  PXRRScreenSize = ^TXRRScreenSize;
  TXRRScreenSize = record
    width, height : cint;
    mwidth, mheight : cint;
  end;

{
   Events.
}

  TXRRScreenChangeNotifyEvent = record
    _type : cint;                 { event base }
    serial : culong;              { # of last request processed by server }
    send_event : TBool;           { true if this came from a SendEvent request }
    display : PDisplay;           { Display the event was read from }
    window : TWindow;             { window which selected for this event }
    root : TWindow;               { Root window for changed screen }
    timestamp : TTime;            { when the screen change occurred }
    config_timestamp : TTime;     { when the last configuration change }
    size_index : TSizeID;
    subpixel_order : TSubpixelOrder;
    rotation : TRotation;
    width : cint;
    height : cint;
    mwidth : cint;
    mheight : cint;
  end;


{ internal representation is private to the library }
  PXRRScreenConfiguration = ^TXRRScreenConfiguration;
  TXRRScreenConfiguration = record end;

T_XRRQueryExtension=function(  dpy : PDisplay;  event_basep,  error_basep : Pcint) : TBoolResult;cdecl;
T_XRRQueryVersion=function(  dpy : PDisplay;  major_versionp : Pcint;  minor_versionp : Pcint) : TStatus;cdecl;
T_XRRGetScreenInfo=function(  dpy : PDisplay;  draw : TDrawable) : PXRRScreenConfiguration;cdecl;
T_XRRFreeScreenConfigInfo=procedure(  config : PXRRScreenConfiguration);cdecl;
T_XRRSetScreenConfig=function(  dpy : PDisplay;  config : PXRRScreenConfiguration;  draw : TDrawable;  size_index : cint;  rotation : TRotation;  timestamp : TTime) : TStatus;cdecl;
T_XRRSetScreenConfigAndRate=function(  dpy : PDisplay;  config : PXRRScreenConfiguration;  draw : TDrawable;  size_index : cint;  rotation : TRotation;  rate : cshort;  timestamp : TTime) : TStatus;cdecl;
T_XRRConfigRotations=function(  config : PXRRScreenConfiguration;  current_rotation : PRotation) : TRotation;cdecl;
T_XRRConfigTimes=function(  config : PXRRScreenConfiguration;  config_timestamp : PTime) : TTime;cdecl;
T_XRRConfigSizes=function(  config : PXRRScreenConfiguration;  nsizes : Pcint) : PXRRScreenSize;cdecl;
T_XRRConfigRates=function(  config : PXRRScreenConfiguration;  sizeID : cint;  nrates : Pcint) : Pcshort;cdecl;
T_XRRConfigCurrentConfiguration=function(  config : PXRRScreenConfiguration;  rotation : PRotation) : TSizeID;cdecl;
T_XRRConfigCurrentRate=function(  config : PXRRScreenConfiguration) : cshort;cdecl;
T_XRRRootToScreen=function(  dpy : PDisplay;  root : TWindow) : cint;cdecl;
T_XRRScreenConfig=function(  dpy : PDisplay;  screen : cint) : PXRRScreenConfiguration;cdecl;
T_XRRConfig=function(  screen : PScreen) : PXRRScreenConfiguration;cdecl;
T_XRRSelectInput=procedure(  dpy : PDisplay;  window : TWindow;  mask : cint);cdecl;
T_XRRRotations=function(  dpy : PDisplay;  screen : cint;  current_rotation : PRotation) : TRotation;cdecl;
T_XRRSizes=function(  dpy : PDisplay;  screen : cint;  nsizes : Pcint) : PXRRScreenSize;cdecl;
T_XRRRates=function(  dpy : PDisplay;  screen : cint;  sizeID : cint;  nrates : Pcint) : Pcshort;cdecl;
T_XRRTimes=function(  dpy : PDisplay;  screen : cint;  config_timestamp : PTime) : TTime;cdecl;
T_XRRUpdateConfiguration=function(  event : PXEvent) : cint;cdecl;

var
XRRQueryExtension : T_XRRQueryExtension;
XRRQueryVersion : T_XRRQueryVersion;
XRRGetScreenInfo : T_XRRGetScreenInfo;
XRRFreeScreenConfigInfo : T_XRRFreeScreenConfigInfo;
XRRSetScreenConfig : T_XRRSetScreenConfig;
XRRSetScreenConfigAndRate : T_XRRSetScreenConfigAndRate;
XRRConfigRotations : T_XRRConfigRotations;
XRRConfigTimes : T_XRRConfigTimes;
XRRConfigSizes : T_XRRConfigSizes;
XRRConfigRates : T_XRRConfigRates;
XRRConfigCurrentConfiguration : T_XRRConfigCurrentConfiguration;
XRRConfigCurrentRate : T_XRRConfigCurrentRate;
XRRRootToScreen : T_XRRRootToScreen;
XRRScreenConfig : T_XRRScreenConfig;
XRRConfig : T_XRRConfig;
XRRSelectInput : T_XRRSelectInput;
XRRRotations : T_XRRRotations;
XRRSizes : T_XRRSizes;
XRRRates : T_XRRRates;
XRRTimes : T_XRRTimes;
XRRUpdateConfiguration : T_XRRUpdateConfiguration;



implementation


procedure init_procvars();
begin
  pointer(XRRQueryExtension) := nil;
  pointer(XRRQueryVersion) := nil;
  pointer(XRRGetScreenInfo) := nil;
  pointer(XRRFreeScreenConfigInfo) := nil;
  pointer(XRRSetScreenConfig) := nil;
  pointer(XRRSetScreenConfigAndRate) := nil;
  pointer(XRRConfigRotations) := nil;
  pointer(XRRConfigTimes) := nil;
  pointer(XRRConfigSizes) := nil;
  pointer(XRRConfigRates) := nil;
  pointer(XRRConfigCurrentConfiguration) := nil;
  pointer(XRRConfigCurrentRate) := nil;
  pointer(XRRRootToScreen) := nil;
  pointer(XRRScreenConfig) := nil;
  pointer(XRRConfig) := nil;
  pointer(XRRSelectInput) := nil;
  pointer(XRRRotations) := nil;
  pointer(XRRSizes) := nil;
  pointer(XRRRates) := nil;
  pointer(XRRTimes) := nil;
  pointer(XRRUpdateConfiguration) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
  pointer(XRRQueryExtension) := GetProcedureAddress(libh,'XRRQueryExtension');
  pointer(XRRQueryVersion) := GetProcedureAddress(libh,'XRRQueryVersion');
  pointer(XRRGetScreenInfo) := GetProcedureAddress(libh,'XRRGetScreenInfo');
  pointer(XRRFreeScreenConfigInfo) := GetProcedureAddress(libh,'XRRFreeScreenConfigInfo');
  pointer(XRRSetScreenConfig) := GetProcedureAddress(libh,'XRRSetScreenConfig');
  pointer(XRRSetScreenConfigAndRate) := GetProcedureAddress(libh,'XRRSetScreenConfigAndRate');
  pointer(XRRConfigRotations) := GetProcedureAddress(libh,'XRRConfigRotations');
  pointer(XRRConfigTimes) := GetProcedureAddress(libh,'XRRConfigTimes');
  pointer(XRRConfigSizes) := GetProcedureAddress(libh,'XRRConfigSizes');
  pointer(XRRConfigRates) := GetProcedureAddress(libh,'XRRConfigRates');
  pointer(XRRConfigCurrentConfiguration) := GetProcedureAddress(libh,'XRRConfigCurrentConfiguration');
  pointer(XRRConfigCurrentRate) := GetProcedureAddress(libh,'XRRConfigCurrentRate');
  pointer(XRRRootToScreen) := GetProcedureAddress(libh,'XRRRootToScreen');
  pointer(XRRScreenConfig) := GetProcedureAddress(libh,'XRRScreenConfig');
  pointer(XRRConfig) := GetProcedureAddress(libh,'XRRConfig');
  pointer(XRRSelectInput) := GetProcedureAddress(libh,'XRRSelectInput');
  pointer(XRRRotations) := GetProcedureAddress(libh,'XRRRotations');
  pointer(XRRSizes) := GetProcedureAddress(libh,'XRRSizes');
  pointer(XRRRates) := GetProcedureAddress(libh,'XRRRates');
  pointer(XRRTimes) := GetProcedureAddress(libh,'XRRTimes');
  pointer(XRRUpdateConfiguration) := GetProcedureAddress(libh,'XRRUpdateConfiguration');
end;


var 
lh : TLibHandle;

function libXrandrAvailable() : boolean;
begin
  libXrandrAvailable := (lh<>NilHandle);
end;

function libXrandrLibHandle() : TLibHandle;
begin
  libXrandrLibHandle := lh;
end;

procedure do_init();
begin
  init_procvars();
  if XlibAvailable() and (lh<>NilHandle) then begin
    load_procvars(lh);
  end;
end;


initialization
begin
  lh := LoadLibrary(libXrandr);
  do_init();
end;

finalization
begin
  if lh<>NilHandle then UnloadLibrary(lh);
end;


end.
