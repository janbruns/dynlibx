# dynlibx

Dynloading Version of most of freepascal/packages/X11, so that Pascal xlib programs can run on systems that don't have Xlib installed.

Can be used as direct replacement (no extra code on application side).

Some few extension modules like Xkbd are missing (feel free to use collect.html to convert them, too).

