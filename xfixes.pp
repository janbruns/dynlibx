(*
 * Copyright (c) 2006, Oracle and/or its affiliates. All rights reserved.
 * Copyright 2011 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *)
(*
 * Copyright © 2002 Keith Packard, member of The XFree86 Project, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Keith Packard not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Keith Packard makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * KEITH PACKARD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL KEITH PACKARD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 *)

unit xfixes;

interface

{$PACKRECORDS C}

uses
  ctypes, x, xlib;

const
  libXfixes = 'libXfixes.so';

{$I xfixeswire.inc}

//#include <X11/Xfuncproto.h>
//#include <X11/Xlib.h>

const
(*
 * This revision number also appears in configure.ac, they have
 * to be manually synchronized
 *)
  XFIXES_REVISION = 1;
  XFIXES_VERSION = (XFIXES_MAJOR * 10000) + (XFIXES_MINOR * 100) + XFIXES_REVISION;

type
  PXFixesSelectionNotifyEvent = ^TXFixesSelectionNotifyEvent;
  TXFixesSelectionNotifyEvent = record
    _type: cint;                        { event base }
    serial: culong;
    send_event: TBool;
    display: PDisplay;
    window: TWindow;
    subtype: cint;
    owner: TWindow;
    selection: TAtom;
    timestamp: TTime;
    selection_timestamp: TTime;
  end;

  PXFixesCursorNotifyEvent = ^TXFixesCursorNotifyEvent;
  TXFixesCursorNotifyEvent = record
    _type: cint;                        { event base }
    serial: culong;
    send_event: TBool;
    display: PDisplay;
    window: TWindow;
    subtype: cint;
    cursor_serial: culong;
    timestamp: TTime;
    cursor_name: TAtom;
  end;

  PXFixesCursorImage = ^TXFixesCursorImage;
  TXFixesCursorImage = record
    x, y: cshort;
    width, height: cushort;
    xhot, yhot: cushort;
    cursor_serial: culong;
    pixels: Pculong;
//#if XFIXES_MAJOR >= 2
    atom: TAtom;                    { Version >= 2 only }
    name: PChar;                    { Version >= 2 only }
//#endif
  end;

//#if XFIXES_MAJOR >= 2
{ Version 2 types }

  PXserverRegion = ^TXserverRegion;
  TXserverRegion = TXID;

  PXFixesCursorImageAndName = ^TXFixesCursorImageAndName;
  TXFixesCursorImageAndName = record
    x, y: cshort;
    width, height: cushort;
    xhot, yhot: cushort;
    cursor_serial: culong;
    pixels: Pculong;
    atom: TAtom;
    name: PChar;
  end;

type
  PPointerBarrier = ^TPointerBarrier;
  TPointerBarrier = TXID;



T_XFixesQueryExtension=function(dpy: PDisplay; event_base_return: Pcint; error_base_return: Pcint): TBoolResult;cdecl;
T_XFixesQueryVersion=function(dpy: PDisplay; major_version_return: Pcint; minor_version_return: Pcint): TStatus;cdecl;
T_XFixesChangeSaveSet=procedure(dpy: PDisplay; win: TWindow; mode: cint; target: cint; map: cint);cdecl;
T_XFixesSelectSelectionInput=procedure(dpy: PDisplay; win: TWindow; selection: TAtom; eventMask: culong);cdecl;
T_XFixesSelectCursorInput=procedure(dpy: PDisplay; win: TWindow; eventMask: culong);cdecl;
T_XFixesGetCursorImage=function(dpy: PDisplay) : PXFixesCursorImage;cdecl;
T_XFixesCreateRegion=function(dpy: PDisplay; rectangles: PXRectangle; nrectangles: cint) : TXserverRegion;cdecl;
T_XFixesCreateRegionFromBitmap=function(dpy: PDisplay; bitmap: TPixmap) : TXserverRegion;cdecl;
T_XFixesCreateRegionFromWindow=function(dpy: PDisplay; window: TWindow; kind: cint) : TXserverRegion;cdecl;
T_XFixesCreateRegionFromGC=function(dpy: PDisplay; gc: TGC) : TXserverRegion;cdecl;
T_XFixesCreateRegionFromPicture=function(dpy: PDisplay; picture: TXID) : TXserverRegion;cdecl;
T_XFixesDestroyRegion=procedure(dpy: PDisplay; region: TXserverRegion);cdecl;
T_XFixesSetRegion=procedure(dpy: PDisplay; region: TXserverRegion; rectangles: PXRectangle; nrectangles: cint);cdecl;
T_XFixesCopyRegion=procedure(dpy: PDisplay; dst: TXserverRegion; src: TXserverRegion);cdecl;
T_XFixesUnionRegion=procedure(dpy: PDisplay; dst: TXserverRegion; src1: TXserverRegion; src2: TXserverRegion);cdecl;
T_XFixesIntersectRegion=procedure(dpy: PDisplay; dst: TXserverRegion; src1: TXserverRegion; src2: TXserverRegion);cdecl;
T_XFixesSubtractRegion=procedure(dpy: PDisplay; dst: TXserverRegion; src1: TXserverRegion; src2: TXserverRegion);cdecl;
T_XFixesInvertRegion=procedure(dpy: PDisplay; dst: TXserverRegion; rect: PXRectangle; src: TXserverRegion);cdecl;
T_XFixesTranslateRegion=procedure(dpy: PDisplay; region: TXserverRegion; dx, dy: cint);cdecl;
T_XFixesRegionExtents=procedure(dpy: PDisplay; dst: TXserverRegion; src: TXserverRegion);cdecl;
T_XFixesFetchRegion=function(dpy: PDisplay; region: TXserverRegion; nrectanglesRet: Pcint) : PXRectangle;cdecl;
T_XFixesFetchRegionAndBounds=function(dpy: PDisplay; region: TXserverRegion; nrectanglesRet: Pcint; bounds: PXRectangle) : PXRectangle;cdecl;
T_XFixesSetGCClipRegion=procedure(dpy: PDisplay; gc: TGC; clip_x_origin: cint; clip_y_origin: cint; region: TXserverRegion);cdecl;
T_XFixesSetWindowShapeRegion=procedure(dpy: PDisplay; win: TWindow; shape_kind: cint; x_off: cint; y_off: cint; region: TXserverRegion);cdecl;
T_XFixesSetPictureClipRegion=procedure(dpy: PDisplay; picture: TXID; clip_x_origin: cint; clip_y_origin: cint; region: TXserverRegion);cdecl;
T_FixesSetCursorName=procedure(dpy: PDisplay; cursor: TCursor; name: PChar);cdecl;
T_XFixesGetCursorName=function(dpy: PDisplay; cursor: TCursor; atom: PAtom) : PChar;cdecl;
T_XFixesChangeCursor=procedure(dpy: PDisplay; source: TCursor; destination: TCursor);cdecl;
T_XFixesChangeCursorByName=procedure(dpy: PDisplay; source: TCursor; name: PChar);cdecl;
T_XFixesExpandRegion=procedure(dpy: PDisplay; dst: TXserverRegion; src: TXserverRegion; left, right, top, bottom: cunsigned);cdecl;
T_XFixesHideCursor=procedure(dpy: PDisplay; win: TWindow);cdecl;
T_XFixesShowCursor=procedure(dpy: PDisplay; win: TWindow);cdecl;
T_XFixesCreatePointerBarrier=function(dpy: PDisplay; w: TWindow; x1, y1, x2, y2: cint; directions: cint; num_devices: cint; devices: Pcint): TPointerBarrier;cdecl;
T_XFixesDestroyPointerBarrier=procedure(dpy: PDisplay; b: TPointerBarrier);cdecl;


var
XFixesQueryExtension : T_XFixesQueryExtension;
XFixesQueryVersion : T_XFixesQueryVersion;
XFixesChangeSaveSet : T_XFixesChangeSaveSet;
XFixesSelectSelectionInput : T_XFixesSelectSelectionInput;
XFixesSelectCursorInput : T_XFixesSelectCursorInput;
XFixesGetCursorImage : T_XFixesGetCursorImage;
XFixesCreateRegion : T_XFixesCreateRegion;
XFixesCreateRegionFromBitmap : T_XFixesCreateRegionFromBitmap;
XFixesCreateRegionFromWindow : T_XFixesCreateRegionFromWindow;
XFixesCreateRegionFromGC : T_XFixesCreateRegionFromGC;
XFixesCreateRegionFromPicture : T_XFixesCreateRegionFromPicture;
XFixesDestroyRegion : T_XFixesDestroyRegion;
XFixesSetRegion : T_XFixesSetRegion;
XFixesCopyRegion : T_XFixesCopyRegion;
XFixesUnionRegion : T_XFixesUnionRegion;
XFixesIntersectRegion : T_XFixesIntersectRegion;
XFixesSubtractRegion : T_XFixesSubtractRegion;
XFixesInvertRegion : T_XFixesInvertRegion;
XFixesTranslateRegion : T_XFixesTranslateRegion;
XFixesRegionExtents : T_XFixesRegionExtents;
XFixesFetchRegion : T_XFixesFetchRegion;
XFixesFetchRegionAndBounds : T_XFixesFetchRegionAndBounds;
XFixesSetGCClipRegion : T_XFixesSetGCClipRegion;
XFixesSetWindowShapeRegion : T_XFixesSetWindowShapeRegion;
XFixesSetPictureClipRegion : T_XFixesSetPictureClipRegion;
FixesSetCursorName : T_FixesSetCursorName;
XFixesGetCursorName : T_XFixesGetCursorName;
XFixesChangeCursor : T_XFixesChangeCursor;
XFixesChangeCursorByName : T_XFixesChangeCursorByName;
XFixesExpandRegion : T_XFixesExpandRegion;
XFixesHideCursor : T_XFixesHideCursor;
XFixesShowCursor : T_XFixesShowCursor;
XFixesCreatePointerBarrier : T_XFixesCreatePointerBarrier;
XFixesDestroyPointerBarrier : T_XFixesDestroyPointerBarrier;



implementation



procedure init_procvars();
begin
  pointer(XFixesQueryExtension) := nil;
  pointer(XFixesQueryVersion) := nil;
  pointer(XFixesChangeSaveSet) := nil;
  pointer(XFixesSelectSelectionInput) := nil;
  pointer(XFixesSelectCursorInput) := nil;
  pointer(XFixesGetCursorImage) := nil;
  pointer(XFixesCreateRegion) := nil;
  pointer(XFixesCreateRegionFromBitmap) := nil;
  pointer(XFixesCreateRegionFromWindow) := nil;
  pointer(XFixesCreateRegionFromGC) := nil;
  pointer(XFixesCreateRegionFromPicture) := nil;
  pointer(XFixesDestroyRegion) := nil;
  pointer(XFixesSetRegion) := nil;
  pointer(XFixesCopyRegion) := nil;
  pointer(XFixesUnionRegion) := nil;
  pointer(XFixesIntersectRegion) := nil;
  pointer(XFixesSubtractRegion) := nil;
  pointer(XFixesInvertRegion) := nil;
  pointer(XFixesTranslateRegion) := nil;
  pointer(XFixesRegionExtents) := nil;
  pointer(XFixesFetchRegion) := nil;
  pointer(XFixesFetchRegionAndBounds) := nil;
  pointer(XFixesSetGCClipRegion) := nil;
  pointer(XFixesSetWindowShapeRegion) := nil;
  pointer(XFixesSetPictureClipRegion) := nil;
  pointer(FixesSetCursorName) := nil;
  pointer(XFixesGetCursorName) := nil;
  pointer(XFixesChangeCursor) := nil;
  pointer(XFixesChangeCursorByName) := nil;
  pointer(XFixesExpandRegion) := nil;
  pointer(XFixesHideCursor) := nil;
  pointer(XFixesShowCursor) := nil;
  pointer(XFixesCreatePointerBarrier) := nil;
  pointer(XFixesDestroyPointerBarrier) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
  pointer(XFixesQueryExtension) := GetProcedureAddress(libh,'XFixesQueryExtension');
  pointer(XFixesQueryVersion) := GetProcedureAddress(libh,'XFixesQueryVersion');
  pointer(XFixesChangeSaveSet) := GetProcedureAddress(libh,'XFixesChangeSaveSet');
  pointer(XFixesSelectSelectionInput) := GetProcedureAddress(libh,'XFixesSelectSelectionInput');
  pointer(XFixesSelectCursorInput) := GetProcedureAddress(libh,'XFixesSelectCursorInput');
  pointer(XFixesGetCursorImage) := GetProcedureAddress(libh,'XFixesGetCursorImage');
  pointer(XFixesCreateRegion) := GetProcedureAddress(libh,'XFixesCreateRegion');
  pointer(XFixesCreateRegionFromBitmap) := GetProcedureAddress(libh,'XFixesCreateRegionFromBitmap');
  pointer(XFixesCreateRegionFromWindow) := GetProcedureAddress(libh,'XFixesCreateRegionFromWindow');
  pointer(XFixesCreateRegionFromGC) := GetProcedureAddress(libh,'XFixesCreateRegionFromGC');
  pointer(XFixesCreateRegionFromPicture) := GetProcedureAddress(libh,'XFixesCreateRegionFromPicture');
  pointer(XFixesDestroyRegion) := GetProcedureAddress(libh,'XFixesDestroyRegion');
  pointer(XFixesSetRegion) := GetProcedureAddress(libh,'XFixesSetRegion');
  pointer(XFixesCopyRegion) := GetProcedureAddress(libh,'XFixesCopyRegion');
  pointer(XFixesUnionRegion) := GetProcedureAddress(libh,'XFixesUnionRegion');
  pointer(XFixesIntersectRegion) := GetProcedureAddress(libh,'XFixesIntersectRegion');
  pointer(XFixesSubtractRegion) := GetProcedureAddress(libh,'XFixesSubtractRegion');
  pointer(XFixesInvertRegion) := GetProcedureAddress(libh,'XFixesInvertRegion');
  pointer(XFixesTranslateRegion) := GetProcedureAddress(libh,'XFixesTranslateRegion');
  pointer(XFixesRegionExtents) := GetProcedureAddress(libh,'XFixesRegionExtents');
  pointer(XFixesFetchRegion) := GetProcedureAddress(libh,'XFixesFetchRegion');
  pointer(XFixesFetchRegionAndBounds) := GetProcedureAddress(libh,'XFixesFetchRegionAndBounds');
  pointer(XFixesSetGCClipRegion) := GetProcedureAddress(libh,'XFixesSetGCClipRegion');
  pointer(XFixesSetWindowShapeRegion) := GetProcedureAddress(libh,'XFixesSetWindowShapeRegion');
  pointer(XFixesSetPictureClipRegion) := GetProcedureAddress(libh,'XFixesSetPictureClipRegion');
  pointer(FixesSetCursorName) := GetProcedureAddress(libh,'FixesSetCursorName');
  pointer(XFixesGetCursorName) := GetProcedureAddress(libh,'XFixesGetCursorName');
  pointer(XFixesChangeCursor) := GetProcedureAddress(libh,'XFixesChangeCursor');
  pointer(XFixesChangeCursorByName) := GetProcedureAddress(libh,'XFixesChangeCursorByName');
  pointer(XFixesExpandRegion) := GetProcedureAddress(libh,'XFixesExpandRegion');
  pointer(XFixesHideCursor) := GetProcedureAddress(libh,'XFixesHideCursor');
  pointer(XFixesShowCursor) := GetProcedureAddress(libh,'XFixesShowCursor');
  pointer(XFixesCreatePointerBarrier) := GetProcedureAddress(libh,'XFixesCreatePointerBarrier');
  pointer(XFixesDestroyPointerBarrier) := GetProcedureAddress(libh,'XFixesDestroyPointerBarrier');
end;


var 
lh : TLibHandle;

function libXfixesAvailable() : boolean;
begin
  libXfixesAvailable := (lh<>NilHandle);
end;

function libXfixesHandle() : TLibHandle;
begin
  libXfixesHandle := lh;
end;

procedure do_init();
begin
  init_procvars();
  if XlibAvailable() and (lh<>NilHandle) then begin
    load_procvars(lh);
  end;
end;


initialization
begin
  lh := LoadLibrary(libXfixes);
  do_init();
end;

finalization
begin
  if lh<>NilHandle then UnloadLibrary(lh);
end;




end.
