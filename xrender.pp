unit xrender;

interface

uses
  x, xlib, ctypes;

const
libXrender='libXrender.so';

function libXrenderAvailable() : boolean;
function libXrenderLibHandle() : TLibHandle;

{
  Automatically converted by H2Pas 0.99.15 from xrender.h
  The following command line parameters were used:
    -p
    -T
    -S
    -d
    -c
    xrender.h
}

{$PACKRECORDS C}

type

   PGlyph = ^TGlyph;
   TGlyph = dword;

   PGlyphSet = ^TGlyphSet;
   TGlyphSet = dword;

   PPicture = ^TPicture;
   TPicture = dword;

   PPictFormat = ^TPictFormat;
   TPictFormat = dword;

const
   RENDER_NAME = 'RENDER';
   RENDER_MAJOR = 0;
   RENDER_MINOR = 0;
   X_RenderQueryVersion = 0;
   X_RenderQueryPictFormats = 1;
   X_RenderQueryPictIndexValues = 2;
   X_RenderQueryDithers = 3;
   X_RenderCreatePicture = 4;
   X_RenderChangePicture = 5;
   X_RenderSetPictureClipRectangles = 6;
   X_RenderFreePicture = 7;
   X_RenderComposite = 8;
   X_RenderScale = 9;
   X_RenderTrapezoids = 10;
   X_RenderTriangles = 11;
   X_RenderTriStrip = 12;
   X_RenderTriFan = 13;
   X_RenderColorTrapezoids = 14;
   X_RenderColorTriangles = 15;
   X_RenderTransform = 16;
   X_RenderCreateGlyphSet = 17;
   X_RenderReferenceGlyphSet = 18;
   X_RenderFreeGlyphSet = 19;
   X_RenderAddGlyphs = 20;
   X_RenderAddGlyphsFromPicture = 21;
   X_RenderFreeGlyphs = 22;
   X_RenderCompositeGlyphs8 = 23;
   X_RenderCompositeGlyphs16 = 24;
   X_RenderCompositeGlyphs32 = 25;
   BadPictFormat = 0;
   BadPicture = 1;
   BadPictOp = 2;
   BadGlyphSet = 3;
   BadGlyph = 4;
   RenderNumberErrors = BadGlyph + 1;
   PictTypeIndexed = 0;
   PictTypeDirect = 1;
   PictOpClear = 0;
   PictOpSrc = 1;
   PictOpDst = 2;
   PictOpOver = 3;
   PictOpOverReverse = 4;
   PictOpIn = 5;
   PictOpInReverse = 6;
   PictOpOut = 7;
   PictOpOutReverse = 8;
   PictOpAtop = 9;
   PictOpAtopReverse = 10;
   PictOpXor = 11;
   PictOpAdd = 12;
   PictOpSaturate = 13;
   PictOpMaximum = 13;
   PolyEdgeSharp = 0;
   PolyEdgeSmooth = 1;
   PolyModePrecise = 0;
   PolyModeImprecise = 1;
   CPRepeat = 1 shl 0;
   CPAlphaMap = 1 shl 1;
   CPAlphaXOrigin = 1 shl 2;
   CPAlphaYOrigin = 1 shl 3;
   CPClipXOrigin = 1 shl 4;
   CPClipYOrigin = 1 shl 5;
   CPClipMask = 1 shl 6;
   CPGraphicsExposure = 1 shl 7;
   CPSubwindowMode = 1 shl 8;
   CPPolyEdge = 1 shl 9;
   CPPolyMode = 1 shl 10;
   CPDither = 1 shl 11;
   CPLastBit = 11;
type

   PXRenderDirectFormat = ^TXRenderDirectFormat;
   TXRenderDirectFormat = record
        red : smallint;
        redMask : smallint;
        green : smallint;
        greenMask : smallint;
        blue : smallint;
        blueMask : smallint;
        alpha : smallint;
        alphaMask : smallint;
     end;

   PXRenderPictFormat = ^TXRenderPictFormat;
   TXRenderPictFormat = record
        id : TPictFormat;
        _type : longint;
        depth : longint;
        direct : TXRenderDirectFormat;
        colormap : TColormap;
     end;

   TXRenderColor = record
     red   : cushort;
     green : cushort;
     blue  : cushort;
     alpha : cushort;
   end;

const
   PictFormatID = 1 shl 0;
   PictFormatType = 1 shl 1;
   PictFormatDepth = 1 shl 2;
   PictFormatRed = 1 shl 3;
   PictFormatRedMask = 1 shl 4;
   PictFormatGreen = 1 shl 5;
   PictFormatGreenMask = 1 shl 6;
   PictFormatBlue = 1 shl 7;
   PictFormatBlueMask = 1 shl 8;
   PictFormatAlpha = 1 shl 9;
   PictFormatAlphaMask = 1 shl 10;
   PictFormatColormap = 1 shl 11;
type

   PXRenderVisual = ^TXRenderVisual;
   TXRenderVisual = record
        visual : PVisual;
        format : PXRenderPictFormat;
     end;

   PXRenderDepth = ^TXRenderDepth;
   TXRenderDepth = record
        depth : longint;
        nvisuals : longint;
        visuals : PXRenderVisual;
     end;

   PXRenderScreen = ^TXRenderScreen;
   TXRenderScreen = record
        depths : PXRenderDepth;
        ndepths : longint;
        fallback : PXRenderPictFormat;
     end;

   PXRenderInfo = ^TXRenderInfo;
   TXRenderInfo = record
        format : PXRenderPictFormat;
        nformat : longint;
        screen : PXRenderScreen;
        nscreen : longint;
        depth : PXRenderDepth;
        ndepth : longint;
        visual : PXRenderVisual;
        nvisual : longint;
     end;

   PXRenderPictureAttributes = ^TXRenderPictureAttributes;
   TXRenderPictureAttributes = record
        _repeat : TBool;
        alpha_map : TPicture;
        alpha_x_origin : longint;
        alpha_y_origin : longint;
        clip_x_origin : longint;
        clip_y_origin : longint;
        clip_mask : TPixmap;
        graphics_exposures : TBool;
        subwindow_mode : longint;
        poly_edge : longint;
        poly_mode : longint;
        dither : TAtom;
     end;

   PXGlyphInfo = ^TXGlyphInfo;
   TXGlyphInfo = record
        width : cushort;
        height : cushort;
        x : cshort;
        y : cshort;
        xOff : cshort;
        yOff : cshort;
     end;

T_XRenderQueryExtension=function(dpy:PDisplay; event_basep:Plongint; error_basep:Plongint):TBoolResult;cdecl;
T_XRenderQueryVersion=function(dpy:PDisplay; major_versionp:Plongint; minor_versionp:Plongint):TStatus;cdecl;
T_XRenderQueryFormats=function(dpy:PDisplay):TStatus;cdecl;
T_XRenderFindVisualFormat=function(dpy:PDisplay; visual:PVisual):PXRenderPictFormat;cdecl;
T_XRenderFindFormat=function(dpy:PDisplay; mask:dword; template:PXRenderPictFormat; count:longint):PXRenderPictFormat;cdecl;
T_XRenderCreatePicture=function(dpy:PDisplay; drawable:TDrawable; format:PXRenderPictFormat; valuemask:dword; attributes:PXRenderPictureAttributes):TPicture;cdecl;
T_XRenderChangePicture=procedure(dpy:PDisplay; picture:TPicture; valuemask:dword; attributes:PXRenderPictureAttributes);cdecl;
T_XRenderFreePicture=procedure(dpy:PDisplay; picture:TPicture);cdecl;
T_XRenderComposite=procedure(dpy:PDisplay; op:longint; src:TPicture; mask:TPicture; dst:TPicture; src_x:longint; src_y:longint; mask_x:longint; mask_y:longint; dst_x:longint;  dst_y:longint; width:dword; height:dword);cdecl;
T_XRenderCreateGlyphSet=function(dpy:PDisplay; format:PXRenderPictFormat):TGlyphSet;cdecl;
T_XRenderReferenceGlyphSet=function(dpy:PDisplay; existing:TGlyphSet):TGlyphSet;cdecl;
T_XRenderFreeGlyphSet=procedure(dpy:PDisplay; glyphset:TGlyphSet);cdecl;
T_XRenderAddGlyphs=procedure(dpy:PDisplay; glyphset:TGlyphSet; gids:PGlyph; glyphs:PXGlyphInfo; nglyphs:longint; images:Pchar; nbyte_images:longint);cdecl;
T_XRenderFreeGlyphs=procedure(dpy:PDisplay; glyphset:TGlyphSet; gids:PGlyph; nglyphs:longint);cdecl;
T_XRenderCompositeString8=procedure(dpy:PDisplay; op:longint; src:TPicture; dst:TPicture; maskFormat:PXRenderPictFormat;          glyphset:TGlyphSet; xSrc:longint; ySrc:longint; xDst:longint; yDst:longint; _string:Pchar; nchar:longint);cdecl;





var
XRenderQueryExtension : T_XRenderQueryExtension;
XRenderQueryVersion : T_XRenderQueryVersion;
XRenderQueryFormats : T_XRenderQueryFormats;
XRenderFindVisualFormat : T_XRenderFindVisualFormat;
XRenderFindFormat : T_XRenderFindFormat;
XRenderCreatePicture : T_XRenderCreatePicture;
XRenderChangePicture : T_XRenderChangePicture;
XRenderFreePicture : T_XRenderFreePicture;
XRenderComposite : T_XRenderComposite;
XRenderCreateGlyphSet : T_XRenderCreateGlyphSet;
XRenderReferenceGlyphSet : T_XRenderReferenceGlyphSet;
XRenderFreeGlyphSet : T_XRenderFreeGlyphSet;
XRenderAddGlyphs : T_XRenderAddGlyphs;
XRenderFreeGlyphs : T_XRenderFreeGlyphs;
XRenderCompositeString8 : T_XRenderCompositeString8;



implementation

procedure init_procvars();
begin
  pointer(XRenderQueryExtension) := nil;
  pointer(XRenderQueryVersion) := nil;
  pointer(XRenderQueryFormats) := nil;
  pointer(XRenderFindVisualFormat) := nil;
  pointer(XRenderFindFormat) := nil;
  pointer(XRenderCreatePicture) := nil;
  pointer(XRenderChangePicture) := nil;
  pointer(XRenderFreePicture) := nil;
  pointer(XRenderComposite) := nil;
  pointer(XRenderCreateGlyphSet) := nil;
  pointer(XRenderReferenceGlyphSet) := nil;
  pointer(XRenderFreeGlyphSet) := nil;
  pointer(XRenderAddGlyphs) := nil;
  pointer(XRenderFreeGlyphs) := nil;
  pointer(XRenderCompositeString8) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
  pointer(XRenderQueryExtension) := GetProcedureAddress(libh,'XRenderQueryExtension');
  pointer(XRenderQueryVersion) := GetProcedureAddress(libh,'XRenderQueryVersion');
  pointer(XRenderQueryFormats) := GetProcedureAddress(libh,'XRenderQueryFormats');
  pointer(XRenderFindVisualFormat) := GetProcedureAddress(libh,'XRenderFindVisualFormat');
  pointer(XRenderFindFormat) := GetProcedureAddress(libh,'XRenderFindFormat');
  pointer(XRenderCreatePicture) := GetProcedureAddress(libh,'XRenderCreatePicture');
  pointer(XRenderChangePicture) := GetProcedureAddress(libh,'XRenderChangePicture');
  pointer(XRenderFreePicture) := GetProcedureAddress(libh,'XRenderFreePicture');
  pointer(XRenderComposite) := GetProcedureAddress(libh,'XRenderComposite');
  pointer(XRenderCreateGlyphSet) := GetProcedureAddress(libh,'XRenderCreateGlyphSet');
  pointer(XRenderReferenceGlyphSet) := GetProcedureAddress(libh,'XRenderReferenceGlyphSet');
  pointer(XRenderFreeGlyphSet) := GetProcedureAddress(libh,'XRenderFreeGlyphSet');
  pointer(XRenderAddGlyphs) := GetProcedureAddress(libh,'XRenderAddGlyphs');
  pointer(XRenderFreeGlyphs) := GetProcedureAddress(libh,'XRenderFreeGlyphs');
  pointer(XRenderCompositeString8) := GetProcedureAddress(libh,'XRenderCompositeString8');
end;


var 
lh : TLibHandle;

function libXrenderAvailable() : boolean;
begin
  libXrenderAvailable := (lh<>NilHandle);
end;

function libXrenderLibHandle() : TLibHandle;
begin
  libXrenderLibHandle := lh;
end;

procedure do_init();
begin
  init_procvars();
  if XlibAvailable() and (lh<>NilHandle) then begin
    load_procvars(lh);
  end;
end;


initialization
begin
  lh := LoadLibrary(libXrender);
  do_init();
end;

finalization
begin
  if lh<>NilHandle then UnloadLibrary(lh);
end;


end.
