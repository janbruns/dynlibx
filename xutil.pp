unit xutil;
interface
uses
  ctypes,x,xlib,keysym;

{$define MACROS}



{
  Automatically converted by H2Pas 0.99.15 from xutil.h
  The following command line parameters were used:
    -p
    -T
    -S
    -d
    -c
    xutil.h
}

{$PACKRECORDS C}

const
   NoValue = $0000;
   XValue = $0001;
   YValue = $0002;
   WidthValue = $0004;
   HeightValue = $0008;
   AllValues = $000F;
   XNegative = $0010;
   YNegative = $0020;
type

   PXSizeHints = ^TXSizeHints;
   TXSizeHints = record
        flags : clong;
        x, y : cint;
        width, height : cint;
        min_width, min_height : cint;
        max_width, max_height : cint;
        width_inc, height_inc : cint;
        min_aspect, max_aspect : record
             x : cint;
             y : cint;
          end;
        base_width, base_height : cint;
        win_gravity : cint;
     end;

const
   USPosition = 1 shl 0;
   USSize = 1 shl 1;
   PPosition = 1 shl 2;
   PSize = 1 shl 3;
   PMinSize = 1 shl 4;
   PMaxSize = 1 shl 5;
   PResizeInc = 1 shl 6;
   PAspect = 1 shl 7;
   PBaseSize = 1 shl 8;
   PWinGravity = 1 shl 9;
   PAllHints = PPosition or PSize or PMinSize or PMaxSize or PResizeInc or PAspect;
type

   PXWMHints = ^TXWMHints;
   TXWMHints = record
        flags : clong;
        input : TBool;
        initial_state : cint;
        icon_pixmap : TPixmap;
        icon_window : TWindow;
        icon_x, icon_y : cint;
        icon_mask : TPixmap;
        window_group : TXID;
     end;

const
   InputHint = 1 shl 0;
   StateHint = 1 shl 1;
   IconPixmapHint = 1 shl 2;
   IconWindowHint = 1 shl 3;
   IconPositionHint = 1 shl 4;
   IconMaskHint = 1 shl 5;
   WindowGroupHint = 1 shl 6;
   AllHints = InputHint or StateHint or IconPixmapHint or IconWindowHint or IconPositionHint or IconMaskHint or WindowGroupHint;
   XUrgencyHint = 1 shl 8;
   WithdrawnState = 0;
   NormalState = 1;
   IconicState = 3;
   DontCareState = 0;
   ZoomState = 2;
   InactiveState = 4;
type

   PXTextProperty = ^TXTextProperty;
   TXTextProperty = record
        value : pcuchar;
        encoding : TAtom;
        format : cint;
        nitems : culong;
     end;

const
   XNoMemory = -1;
   XLocaleNotSupported = -2;
   XConverterNotFound = -3;
type

   PXICCEncodingStyle = ^TXICCEncodingStyle;
   TXICCEncodingStyle = (XStringStyle,XCompoundTextStyle,XTextStyle,
     XStdICCTextStyle,XUTF8StringStyle);

   PPXIconSize = ^PXIconSize;
   PXIconSize = ^TXIconSize;
   TXIconSize = record
        min_width, min_height : cint;
        max_width, max_height : cint;
        width_inc, height_inc : cint;
     end;

   PXClassHint = ^TXClassHint;
   TXClassHint = record
        res_name : Pchar;
        res_class : Pchar;
     end;

type

   PXComposeStatus = ^TXComposeStatus;
   TXComposeStatus = record
        compose_ptr : TXPointer;
        chars_matched : cint;
     end;

type

   PXRegion = ^TXRegion;
   TXRegion = record
     end;
   TRegion = PXRegion;
   PRegion = ^TRegion;

const
   RectangleOut = 0;
   RectangleIn = 1;
   RectanglePart = 2;
type

   PXVisualInfo = ^TXVisualInfo;
   TXVisualInfo = record
        visual : PVisual;
        visualid : TVisualID;
        screen : cint;
        depth : cint;
        _class : cint;
        red_mask : culong;
        green_mask : culong;
        blue_mask : culong;
        colormap_size : cint;
        bits_per_rgb : cint;
     end;

const
   VisualNoMask = $0;
   VisualIDMask = $1;
   VisualScreenMask = $2;
   VisualDepthMask = $4;
   VisualClassMask = $8;
   VisualRedMaskMask = $10;
   VisualGreenMaskMask = $20;
   VisualBlueMaskMask = $40;
   VisualColormapSizeMask = $80;
   VisualBitsPerRGBMask = $100;
   VisualAllMask = $1FF;
type

   PPXStandardColormap = ^PXStandardColormap;
   PXStandardColormap = ^TXStandardColormap;
   TXStandardColormap = record
        colormap : TColormap;
        red_max : culong;
        red_mult : culong;
        green_max : culong;
        green_mult : culong;
        blue_max : culong;
        blue_mult : culong;
        base_pixel : culong;
        visualid : TVisualID;
        killid : TXID;
     end;

const
   BitmapSuccess = 0;
   BitmapOpenFailed = 1;
   BitmapFileInvalid = 2;
   BitmapNoMemory = 3;
   XCSUCCESS = 0;
   XCNOMEM = 1;
   XCNOENT = 2;
   ReleaseByFreeingColormap : TXID = TXID(1);

type
   PXContext = ^TXContext;
   TXContext = cint;

T_XClipBox=function(para1:TRegion; para2:PXRectangle):cint;cdecl;
T_XDeleteContext=function(para1:PDisplay; para2:TXID; para3:TXContext):cint;cdecl;
T_XDestroyRegion=function(para1:TRegion):cint;cdecl;
T_XEmptyRegion=function(para1:TRegion):cint;cdecl;
T_XEqualRegion=function(para1:TRegion; para2:TRegion):cint;cdecl;
T_XFindContext=function(para1:PDisplay; para2:TXID; para3:TXContext; para4:PXPointer):cint;cdecl;
T_XGetClassHint=function(para1:PDisplay; para2:TWindow; para3:PXClassHint):TStatus;cdecl;
T_XGetIconSizes=function(para1:PDisplay; para2:TWindow; para3:PPXIconSize; para4:Pcint):TStatus;cdecl;
T_XGetNormalHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints):TStatus;cdecl;
T_XGetRGBColormaps=function(para1:PDisplay; para2:TWindow; para3:PPXStandardColormap; para4:Pcint; para5:TAtom):TStatus;cdecl;
T_XGetSizeHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints; para4:TAtom):TStatus;cdecl;
T_XGetStandardColormap=function(para1:PDisplay; para2:TWindow; para3:PXStandardColormap; para4:TAtom):TStatus;cdecl;
T_XGetTextProperty=function(para1:PDisplay; para2:TWindow; para3:PXTextProperty; para4:TAtom):TStatus;cdecl;
T_XGetVisualInfo=function(para1:PDisplay; para2:clong; para3:PXVisualInfo; para4:Pcint):PXVisualInfo;cdecl;
T_XGetWMClientMachine=function(para1:PDisplay; para2:TWindow; para3:PXTextProperty):TStatus;cdecl;
T_XGetWMHints=function(para1:PDisplay; para2:TWindow):PXWMHints;cdecl;
T_XGetWMIconName=function(para1:PDisplay; para2:TWindow; para3:PXTextProperty):TStatus;cdecl;
T_XGetWMName=function(para1:PDisplay; para2:TWindow; para3:PXTextProperty):TStatus;cdecl;
T_XGetWMNormalHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints; para4:Pclong):TStatus;cdecl;
T_XGetWMSizeHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints; para4:Pclong; para5:TAtom):TStatus;cdecl;
T_XGetZoomHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints):TStatus;cdecl;
T_XIntersectRegion=function(para1:TRegion; para2:TRegion; para3:TRegion):cint;cdecl;
T_XConvertCase=procedure(para1:TKeySym; para2:PKeySym; para3:PKeySym);cdecl;
T_XLookupString=function(para1:PXKeyEvent; para2:Pchar; para3:cint; para4:PKeySym; para5:PXComposeStatus):cint;cdecl;
T_XMatchVisualInfo=function(para1:PDisplay; para2:cint; para3:cint; para4:cint; para5:PXVisualInfo):TStatus;cdecl;
T_XOffsetRegion=function(para1:TRegion; para2:cint; para3:cint):cint;cdecl;
T_XPointInRegion=function(para1:TRegion; para2:cint; para3:cint):TBoolResult;cdecl;
T_XPolygonRegion=function(para1:PXPoint; para2:cint; para3:cint):TRegion;cdecl;
T_XRectInRegion=function(para1:TRegion; para2:cint; para3:cint; para4:cuint; para5:cuint):cint;cdecl;
T_XSaveContext=function(para1:PDisplay; para2:TXID; para3:TXContext; para4:Pchar):cint;cdecl;
T_XSetClassHint=function(para1:PDisplay; para2:TWindow; para3:PXClassHint):cint;cdecl;
T_XSetIconSizes=function(para1:PDisplay; para2:TWindow; para3:PXIconSize; para4:cint):cint;cdecl;
T_XSetNormalHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints):cint;cdecl;
T_XSetRGBColormaps=procedure(para1:PDisplay; para2:TWindow; para3:PXStandardColormap; para4:cint; para5:TAtom);cdecl;
T_XSetSizeHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints; para4:TAtom):cint;cdecl;
T_XSetStandardProperties=function(para1:PDisplay; para2:TWindow; para3:Pchar; para4:Pchar; para5:TPixmap; para6:PPchar; para7:cint; para8:PXSizeHints):cint;cdecl;
T_XSetTextProperty=procedure(para1:PDisplay; para2:TWindow; para3:PXTextProperty; para4:TAtom);cdecl;
T_XSetWMClientMachine=procedure(para1:PDisplay; para2:TWindow; para3:PXTextProperty);cdecl;
T_XSetWMHints=function(para1:PDisplay; para2:TWindow; para3:PXWMHints):cint;cdecl;
T_XSetWMIconName=procedure(para1:PDisplay; para2:TWindow; para3:PXTextProperty);cdecl;
T_XSetWMName=procedure(para1:PDisplay; para2:TWindow; para3:PXTextProperty);cdecl;
T_XSetWMNormalHints=procedure(ADisplay:PDisplay; AWindow:TWindow; AHints:PXSizeHints);cdecl;
T_XSetWMProperties=procedure(ADisplay:PDisplay; AWindow:TWindow; AWindowName:PXTextProperty; AIconName:PXTextProperty; AArgv:PPchar; AArgc:cint; ANormalHints:PXSizeHints; AWMHints:PXWMHints; AClassHints:PXClassHint);cdecl;
T_XmbSetWMProperties=procedure(para1:PDisplay; para2:TWindow; para3:Pchar; para4:Pchar; para5:PPchar; para6:cint; para7:PXSizeHints; para8:PXWMHints; para9:PXClassHint);cdecl;
T_Xutf8SetWMProperties=procedure(para1:PDisplay; para2:TWindow; para3:Pchar; para4:Pchar; para5:PPchar; para6:cint; para7:PXSizeHints; para8:PXWMHints; para9:PXClassHint);cdecl;
T_XSetWMSizeHints=procedure(para1:PDisplay; para2:TWindow; para3:PXSizeHints; para4:TAtom);cdecl;
T_XSetRegion=function(para1:PDisplay; para2:TGC; para3:TRegion):cint;cdecl;
T_XSetStandardColormap=procedure(para1:PDisplay; para2:TWindow; para3:PXStandardColormap; para4:TAtom);cdecl;
T_XSetZoomHints=function(para1:PDisplay; para2:TWindow; para3:PXSizeHints):cint;cdecl;
T_XShrinkRegion=function(para1:TRegion; para2:cint; para3:cint):cint;cdecl;
T_XStringListToTextProperty=function(para1:PPchar; para2:cint; para3:PXTextProperty):TStatus;cdecl;
T_XSubtractRegion=function(para1:TRegion; para2:TRegion; para3:TRegion):cint;cdecl;
T_XmbTextListToTextProperty=function(para1:PDisplay; para2:PPchar; para3:cint; para4:TXICCEncodingStyle; para5:PXTextProperty):cint;cdecl;
T_XwcTextListToTextProperty=function(para1:PDisplay; para2:PPWideChar; para3:cint; para4:TXICCEncodingStyle; para5:PXTextProperty):cint;cdecl;
T_Xutf8TextListToTextProperty=function(para1:PDisplay; para2:PPchar; para3:cint; para4:TXICCEncodingStyle; para5:PXTextProperty):cint;cdecl;
T_XwcFreeStringList=procedure(para1:PPWideChar);cdecl;
T_XTextPropertyToStringList=function(para1:PXTextProperty; para2:PPPchar; para3:Pcint):TStatus;cdecl;
T_XmbTextPropertyToTextList=function(para1:PDisplay; para2:PXTextProperty; para3:PPPchar; para4:Pcint):cint;cdecl;
T_XwcTextPropertyToTextList=function(para1:PDisplay; para2:PXTextProperty; para3:PPPWideChar; para4:Pcint):cint;cdecl;
T_Xutf8TextPropertyToTextList=function(para1:PDisplay; para2:PXTextProperty; para3:PPPchar; para4:Pcint):cint;cdecl;
T_XUnionRectWithRegion=function(para1:PXRectangle; para2:TRegion; para3:TRegion):cint;cdecl;
T_XUnionRegion=function(para1:TRegion; para2:TRegion; para3:TRegion):cint;cdecl;
T_XWMGeometry=function(para1:PDisplay; para2:cint; para3:Pchar; para4:Pchar; para5:cuint; para6:PXSizeHints; para7:Pcint; para8:Pcint; para9:Pcint; para10:Pcint; para11:Pcint):cint;cdecl;
T_XXorRegion=function(para1:TRegion; para2:TRegion; para3:TRegion):cint;cdecl;

var
XClipBox : T_XClipBox;
XDeleteContext : T_XDeleteContext;
XDestroyRegion : T_XDestroyRegion;
XEmptyRegion : T_XEmptyRegion;
XEqualRegion : T_XEqualRegion;
XFindContext : T_XFindContext;
XGetClassHint : T_XGetClassHint;
XGetIconSizes : T_XGetIconSizes;
XGetNormalHints : T_XGetNormalHints;
XGetRGBColormaps : T_XGetRGBColormaps;
XGetSizeHints : T_XGetSizeHints;
XGetStandardColormap : T_XGetStandardColormap;
XGetTextProperty : T_XGetTextProperty;
XGetVisualInfo : T_XGetVisualInfo;
XGetWMClientMachine : T_XGetWMClientMachine;
XGetWMHints : T_XGetWMHints;
XGetWMIconName : T_XGetWMIconName;
XGetWMName : T_XGetWMName;
XGetWMNormalHints : T_XGetWMNormalHints;
XGetWMSizeHints : T_XGetWMSizeHints;
XGetZoomHints : T_XGetZoomHints;
XIntersectRegion : T_XIntersectRegion;
XConvertCase : T_XConvertCase;
XLookupString : T_XLookupString;
XMatchVisualInfo : T_XMatchVisualInfo;
XOffsetRegion : T_XOffsetRegion;
XPointInRegion : T_XPointInRegion;
XPolygonRegion : T_XPolygonRegion;
XRectInRegion : T_XRectInRegion;
XSaveContext : T_XSaveContext;
XSetClassHint : T_XSetClassHint;
XSetIconSizes : T_XSetIconSizes;
XSetNormalHints : T_XSetNormalHints;
XSetRGBColormaps : T_XSetRGBColormaps;
XSetSizeHints : T_XSetSizeHints;
XSetStandardProperties : T_XSetStandardProperties;
XSetTextProperty : T_XSetTextProperty;
XSetWMClientMachine : T_XSetWMClientMachine;
XSetWMHints : T_XSetWMHints;
XSetWMIconName : T_XSetWMIconName;
XSetWMName : T_XSetWMName;
XSetWMNormalHints : T_XSetWMNormalHints;
XSetWMProperties : T_XSetWMProperties;
XmbSetWMProperties : T_XmbSetWMProperties;
Xutf8SetWMProperties : T_Xutf8SetWMProperties;
XSetWMSizeHints : T_XSetWMSizeHints;
XSetRegion : T_XSetRegion;
XSetStandardColormap : T_XSetStandardColormap;
XSetZoomHints : T_XSetZoomHints;
XShrinkRegion : T_XShrinkRegion;
XStringListToTextProperty : T_XStringListToTextProperty;
XSubtractRegion : T_XSubtractRegion;
XmbTextListToTextProperty : T_XmbTextListToTextProperty;
XwcTextListToTextProperty : T_XwcTextListToTextProperty;
Xutf8TextListToTextProperty : T_Xutf8TextListToTextProperty;
XwcFreeStringList : T_XwcFreeStringList;
XTextPropertyToStringList : T_XTextPropertyToStringList;
XmbTextPropertyToTextList : T_XmbTextPropertyToTextList;
XwcTextPropertyToTextList : T_XwcTextPropertyToTextList;
Xutf8TextPropertyToTextList : T_Xutf8TextPropertyToTextList;
XUnionRectWithRegion : T_XUnionRectWithRegion;
XUnionRegion : T_XUnionRegion;
XWMGeometry : T_XWMGeometry;
XXorRegion : T_XXorRegion;



{$ifdef MACROS}
function XDestroyImage(ximage : PXImage) : cint;
function XGetPixel(ximage : PXImage; x, y : cint) : culong;
function XPutPixel(ximage : PXImage; x, y : cint; pixel : culong) : cint;
function XSubImage(ximage : PXImage; x, y : cint; width, height : cuint) : PXImage;
function XAddPixel(ximage : PXImage; value : clong) : cint;
function IsKeypadKey(keysym : TKeySym) : Boolean;
function IsPrivateKeypadKey(keysym : TKeySym) : Boolean;
function IsCursorKey(keysym : TKeySym) : Boolean;
function IsPFKey(keysym : TKeySym) : Boolean;
function IsFunctionKey(keysym : TKeySym) : Boolean;
function IsMiscFunctionKey(keysym : TKeySym) : Boolean;
function IsModifierKey(keysym : TKeySym) : Boolean;
{function XUniqueContext : TXContext;
function XStringToContext(_string : Pchar) : TXContext;}
{$endif MACROS}

implementation
uses dynlibs;

{$ifdef MACROS}

function XDestroyImage(ximage : PXImage) : cint;

begin
  XDestroyImage := ximage^.f.destroy_image(ximage);
end;

function XGetPixel(ximage : PXImage; x, y : cint) : culong;
begin
   XGetPixel:=ximage^.f.get_pixel(ximage, x, y);
end;

function XPutPixel(ximage : PXImage; x, y : cint; pixel : culong) : cint;
begin
   XPutPixel:=ximage^.f.put_pixel(ximage, x, y, pixel);
end;

function XSubImage(ximage : PXImage; x, y : cint; width, height : cuint) : PXImage;
begin
   XSubImage:=ximage^.f.sub_image(ximage, x, y, width, height);
end;

function XAddPixel(ximage : PXImage; value : clong) : cint;
begin
   XAddPixel:=ximage^.f.add_pixel(ximage, value);
end;

function IsKeypadKey(keysym : TKeySym) : Boolean;
begin
   IsKeypadKey:=(keysym >= XK_KP_Space) and (keysym <= XK_KP_Equal);
end;

function IsPrivateKeypadKey(keysym : TKeySym) : Boolean;
begin
   IsPrivateKeypadKey:=(keysym >= $11000000) and (keysym <= $1100FFFF);
end;

function IsCursorKey(keysym : TKeySym) : Boolean;
begin
   IsCursorKey:=(keysym >= XK_Home) and (keysym < XK_Select);
end;

function IsPFKey(keysym : TKeySym) : Boolean;
begin
   IsPFKey:=(keysym >= XK_KP_F1) and (keysym <= XK_KP_F4);
end;

function IsFunctionKey(keysym : TKeySym) : Boolean;
begin
   IsFunctionKey:=(keysym >= XK_F1) and (keysym <= XK_F35);
end;

function IsMiscFunctionKey(keysym : TKeySym) : Boolean;
begin
   IsMiscFunctionKey:=(keysym >= XK_Select) and (keysym <= XK_Break);
end;

function IsModifierKey(keysym : TKeySym) : Boolean;
begin
  IsModifierKey := ((keysym >= XK_Shift_L) And (keysym <= XK_Hyper_R)) Or
                   (keysym = XK_Mode_switch) Or (keysym = XK_Num_Lock);
end;

{...needs xresource
function XUniqueContext : TXContext;
begin
   XUniqueContext:=TXContext(XrmUniqueQuark);
end;

function XStringToContext(_string : Pchar) : TXContext;
begin
   XStringToContext:=TXContext(XrmStringToQuark(_string));
end;}
{$endif MACROS}



procedure init_procvars();
begin
  pointer(XClipBox) := nil;
  pointer(XDeleteContext) := nil;
  pointer(XDestroyRegion) := nil;
  pointer(XEmptyRegion) := nil;
  pointer(XEqualRegion) := nil;
  pointer(XFindContext) := nil;
  pointer(XGetClassHint) := nil;
  pointer(XGetIconSizes) := nil;
  pointer(XGetNormalHints) := nil;
  pointer(XGetRGBColormaps) := nil;
  pointer(XGetSizeHints) := nil;
  pointer(XGetStandardColormap) := nil;
  pointer(XGetTextProperty) := nil;
  pointer(XGetVisualInfo) := nil;
  pointer(XGetWMClientMachine) := nil;
  pointer(XGetWMHints) := nil;
  pointer(XGetWMIconName) := nil;
  pointer(XGetWMName) := nil;
  pointer(XGetWMNormalHints) := nil;
  pointer(XGetWMSizeHints) := nil;
  pointer(XGetZoomHints) := nil;
  pointer(XIntersectRegion) := nil;
  pointer(XConvertCase) := nil;
  pointer(XLookupString) := nil;
  pointer(XMatchVisualInfo) := nil;
  pointer(XOffsetRegion) := nil;
  pointer(XPointInRegion) := nil;
  pointer(XPolygonRegion) := nil;
  pointer(XRectInRegion) := nil;
  pointer(XSaveContext) := nil;
  pointer(XSetClassHint) := nil;
  pointer(XSetIconSizes) := nil;
  pointer(XSetNormalHints) := nil;
  pointer(XSetRGBColormaps) := nil;
  pointer(XSetSizeHints) := nil;
  pointer(XSetStandardProperties) := nil;
  pointer(XSetTextProperty) := nil;
  pointer(XSetWMClientMachine) := nil;
  pointer(XSetWMHints) := nil;
  pointer(XSetWMIconName) := nil;
  pointer(XSetWMName) := nil;
  pointer(XSetWMNormalHints) := nil;
  pointer(XSetWMProperties) := nil;
  pointer(XmbSetWMProperties) := nil;
  pointer(Xutf8SetWMProperties) := nil;
  pointer(XSetWMSizeHints) := nil;
  pointer(XSetRegion) := nil;
  pointer(XSetStandardColormap) := nil;
  pointer(XSetZoomHints) := nil;
  pointer(XShrinkRegion) := nil;
  pointer(XStringListToTextProperty) := nil;
  pointer(XSubtractRegion) := nil;
  pointer(XmbTextListToTextProperty) := nil;
  pointer(XwcTextListToTextProperty) := nil;
  pointer(Xutf8TextListToTextProperty) := nil;
  pointer(XwcFreeStringList) := nil;
  pointer(XTextPropertyToStringList) := nil;
  pointer(XmbTextPropertyToTextList) := nil;
  pointer(XwcTextPropertyToTextList) := nil;
  pointer(Xutf8TextPropertyToTextList) := nil;
  pointer(XUnionRectWithRegion) := nil;
  pointer(XUnionRegion) := nil;
  pointer(XWMGeometry) := nil;
  pointer(XXorRegion) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
  pointer(XClipBox) := GetProcedureAddress(libh,'XClipBox');
  pointer(XDeleteContext) := GetProcedureAddress(libh,'XDeleteContext');
  pointer(XDestroyRegion) := GetProcedureAddress(libh,'XDestroyRegion');
  pointer(XEmptyRegion) := GetProcedureAddress(libh,'XEmptyRegion');
  pointer(XEqualRegion) := GetProcedureAddress(libh,'XEqualRegion');
  pointer(XFindContext) := GetProcedureAddress(libh,'XFindContext');
  pointer(XGetClassHint) := GetProcedureAddress(libh,'XGetClassHint');
  pointer(XGetIconSizes) := GetProcedureAddress(libh,'XGetIconSizes');
  pointer(XGetNormalHints) := GetProcedureAddress(libh,'XGetNormalHints');
  pointer(XGetRGBColormaps) := GetProcedureAddress(libh,'XGetRGBColormaps');
  pointer(XGetSizeHints) := GetProcedureAddress(libh,'XGetSizeHints');
  pointer(XGetStandardColormap) := GetProcedureAddress(libh,'XGetStandardColormap');
  pointer(XGetTextProperty) := GetProcedureAddress(libh,'XGetTextProperty');
  pointer(XGetVisualInfo) := GetProcedureAddress(libh,'XGetVisualInfo');
  pointer(XGetWMClientMachine) := GetProcedureAddress(libh,'XGetWMClientMachine');
  pointer(XGetWMHints) := GetProcedureAddress(libh,'XGetWMHints');
  pointer(XGetWMIconName) := GetProcedureAddress(libh,'XGetWMIconName');
  pointer(XGetWMName) := GetProcedureAddress(libh,'XGetWMName');
  pointer(XGetWMNormalHints) := GetProcedureAddress(libh,'XGetWMNormalHints');
  pointer(XGetWMSizeHints) := GetProcedureAddress(libh,'XGetWMSizeHints');
  pointer(XGetZoomHints) := GetProcedureAddress(libh,'XGetZoomHints');
  pointer(XIntersectRegion) := GetProcedureAddress(libh,'XIntersectRegion');
  pointer(XConvertCase) := GetProcedureAddress(libh,'XConvertCase');
  pointer(XLookupString) := GetProcedureAddress(libh,'XLookupString');
  pointer(XMatchVisualInfo) := GetProcedureAddress(libh,'XMatchVisualInfo');
  pointer(XOffsetRegion) := GetProcedureAddress(libh,'XOffsetRegion');
  pointer(XPointInRegion) := GetProcedureAddress(libh,'XPointInRegion');
  pointer(XPolygonRegion) := GetProcedureAddress(libh,'XPolygonRegion');
  pointer(XRectInRegion) := GetProcedureAddress(libh,'XRectInRegion');
  pointer(XSaveContext) := GetProcedureAddress(libh,'XSaveContext');
  pointer(XSetClassHint) := GetProcedureAddress(libh,'XSetClassHint');
  pointer(XSetIconSizes) := GetProcedureAddress(libh,'XSetIconSizes');
  pointer(XSetNormalHints) := GetProcedureAddress(libh,'XSetNormalHints');
  pointer(XSetRGBColormaps) := GetProcedureAddress(libh,'XSetRGBColormaps');
  pointer(XSetSizeHints) := GetProcedureAddress(libh,'XSetSizeHints');
  pointer(XSetStandardProperties) := GetProcedureAddress(libh,'XSetStandardProperties');
  pointer(XSetTextProperty) := GetProcedureAddress(libh,'XSetTextProperty');
  pointer(XSetWMClientMachine) := GetProcedureAddress(libh,'XSetWMClientMachine');
  pointer(XSetWMHints) := GetProcedureAddress(libh,'XSetWMHints');
  pointer(XSetWMIconName) := GetProcedureAddress(libh,'XSetWMIconName');
  pointer(XSetWMName) := GetProcedureAddress(libh,'XSetWMName');
  pointer(XSetWMNormalHints) := GetProcedureAddress(libh,'XSetWMNormalHints');
  pointer(XSetWMProperties) := GetProcedureAddress(libh,'XSetWMProperties');
  pointer(XmbSetWMProperties) := GetProcedureAddress(libh,'XmbSetWMProperties');
  pointer(Xutf8SetWMProperties) := GetProcedureAddress(libh,'Xutf8SetWMProperties');
  pointer(XSetWMSizeHints) := GetProcedureAddress(libh,'XSetWMSizeHints');
  pointer(XSetRegion) := GetProcedureAddress(libh,'XSetRegion');
  pointer(XSetStandardColormap) := GetProcedureAddress(libh,'XSetStandardColormap');
  pointer(XSetZoomHints) := GetProcedureAddress(libh,'XSetZoomHints');
  pointer(XShrinkRegion) := GetProcedureAddress(libh,'XShrinkRegion');
  pointer(XStringListToTextProperty) := GetProcedureAddress(libh,'XStringListToTextProperty');
  pointer(XSubtractRegion) := GetProcedureAddress(libh,'XSubtractRegion');
  pointer(XmbTextListToTextProperty) := GetProcedureAddress(libh,'XmbTextListToTextProperty');
  pointer(XwcTextListToTextProperty) := GetProcedureAddress(libh,'XwcTextListToTextProperty');
  pointer(Xutf8TextListToTextProperty) := GetProcedureAddress(libh,'Xutf8TextListToTextProperty');
  pointer(XwcFreeStringList) := GetProcedureAddress(libh,'XwcFreeStringList');
  pointer(XTextPropertyToStringList) := GetProcedureAddress(libh,'XTextPropertyToStringList');
  pointer(XmbTextPropertyToTextList) := GetProcedureAddress(libh,'XmbTextPropertyToTextList');
  pointer(XwcTextPropertyToTextList) := GetProcedureAddress(libh,'XwcTextPropertyToTextList');
  pointer(Xutf8TextPropertyToTextList) := GetProcedureAddress(libh,'Xutf8TextPropertyToTextList');
  pointer(XUnionRectWithRegion) := GetProcedureAddress(libh,'XUnionRectWithRegion');
  pointer(XUnionRegion) := GetProcedureAddress(libh,'XUnionRegion');
  pointer(XWMGeometry) := GetProcedureAddress(libh,'XWMGeometry');
  pointer(XXorRegion) := GetProcedureAddress(libh,'XXorRegion');
end;


procedure do_init();
begin
  init_procvars();
  if XlibAvailable() then begin
    load_procvars(XlibLibHandle());
  end;
end;


initialization
begin
  do_init();
end;

finalization
begin
end;


end.
